# borg

#### Table of Contents

1. [Description](#description)
2. [Setup](#setup)
3. [Usage](#usage)
4. [Reference](#reference)
5. [Limitations](#limitations)

## Description

This module configures a backup script using borg and setups a cron job
accordingly.

The design of this module is modular: you can call the `borg::backup` define as
many times you want to create different backup schemes.

## Setup

This module takes for granted that:

* You have already created a borg archive somewhere using `borg init`.
* The user you backup with has access to the relevant files to backup from and
  to the borg archive to backup to, be it locally or through `ssh`.

Be aware that the backup script created will contain your archive encryption
passphrase. This file is only readable by the user running the backup job.

## Usage

The simplest way to use this module is this way:

    include borg

    borg::backup {
      'root':
        archive_path => 'webserver',
        archive_path => '/data/borg/my-backup',
        passphrase   => '12345-super-strong',
        type         => 'borg';
    }

An advanced example might look like:

    include borg

    borg::backup {
      'kvm':
        backup_path  => '/',
        archive_path => 'kvm',
        archive_path => 'foo@bar:/data/borg/kvm',
        passphrase   => '12345-super-strong',
        exclude      => [ '/proc', '/sys', '/var/lib/libvirt/images', '/mnt' ],
        prune        => true,
        daily        => 7,
        weekly       => 4,
        monthly      => 0,
        log          => true,
        type         => 'sshfs',
        sshfs_mount  => '/data/foobar',
        cron_time    => '0 2 * * *';

      'mnt':
        backup_path  => '/mnt',
        archive_path => 'server-number-42',
        archive_path => '/data/borg/kvm-mnt',
        passphrase   => '12345-super-strong',
        type         => 'borg',
        prune        => false,
        user         => 'mounty-mounter';
    }

If you want to use this module using Hiera, you will have to add a
`create_ressources` statement somewhere in your manifests. You can use something
like this:

    $borg_backup = hiera_hash('borg::backup',{})
    create_resources('borg::backup',$borg_backup)

A simple Hiera syntax for a backup definition would look like this:

    borg::backup:
      'root':
        archive_name: webserver
        archive_path: /data/borg/my-backup
        passphrase: 12345-super-strong
        type: borg

## Reference

### Classes

#### Public classes

* `borg`: Main class. Used for installs and directory creation.

#### Private classes

* `borg::backup`: Handles the creation of backup scripts and cron jobs.

### Parameters

The following parameters are available in the `borg::backup` class:

#### `backup_path`

Optional.

Data type: String.

Path to the files to backup.

Default value: `/`.

#### `archive_name`

Data type: String.

Name of the borg archive.

Default value: `undef`.

#### `archive_path`

Data type: String.

Path to the borg archive to backup to. Can either be a local path or an ssh
remote path.

Default value: `undef`.

#### `passphrase`

Optional.

Data type: String.

Passphrase for the borg archive. If not set, it is taken from granted the borg
archive was created without a passphrase.

Default value: `undef`.

#### `exclude`

Optional.

Data type: Array.

Paths to exclude from the backup.

Default value: `'/proc', '/sys'`.

#### `prune`

Optional.

Data type: Boolean.

Using this parameter will prune your backups at the rate set by the `daily`,
`weekly`, `monthly` and `yearly` values.

Default value: `true`.

#### `daily`

Optional.

Data type: Integer.

Numbers of days to keep a backup before pruning.

Default value: `7`.

#### `weekly`

Optional.

Data type: Integer.

Numbers of weeks to keep a backup before pruning.

Default value: `4`.

#### `monthly`

Optional.

Data type: Integer.

Numbers of months to keep a backup before pruning.

Default value: `6`.

#### `yearly`

Optional.

Data type: Integer.

Numbers of years to keep a backup before pruning.

Default value: `0`.

#### `log`

Optional.

Data type: Boolean.

This parameter is used to log the exit code of the last backup run. If the
`prune` parameter is set, it will also log the exit code of the last prune run.

This is useful if you want to monitor the success of your backup runs in
multiple ways, like [check_borg][] does.

Log files will be written to `/var/log/borg/borg-${name}.log` and will look like:

    create: 0
    prune: 0

Default value: `false`.

[check_borg]: https://gitlab.com/baldurmen/check_borg

#### `type`

Optional.

Data type: Enum.

Two types of backups are supported: `borg` and `sshfs`.

If the former is used, the backup run takes for granted `borg` is installed on
the endpoint and can server the files, either locally or through `ssh`.

If the latter is used, the archive path will be mounted on the path specified by
the `sshfs_mount` parameter with `sshfs`. In that case, the mounting point will
be unmounted at the end of the backup run.

#### `sshfs_mount`

Optional.

Data type: String.

The mounting point to used when `sshfs` is set as the backup type.

Default value: `/mnt`.

#### `user`

Optional.

Data type: String.

The user to run the backup as.

Default value: `root`.

#### `cron_time`

Optional.

Data type: String.

When the cron job will run. This should respect the cron time format.

Default value: `0 1 * * *`.

## Limitations

This module was developed for Debian Stretch and Puppet 4. Compatibility on
other OS or Puppet versions is not guaranteed.
