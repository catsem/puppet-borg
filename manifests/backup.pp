define borg::backup (
  String  $backup_path  = '/',
  String  $archive_name = undef,
  String  $archive_path = undef,
  Variant[String, Undef] $passphrase = undef,
  Array   $exclude      = [ '/proc', '/sys' ],
  Boolean $prune        = true,
  Integer $daily        = 7,
  Integer $weekly       = 4,
  Integer $monthly      = 6,
  Integer $yearly       = 0,
  Boolean $log          = false,
  Enum    $type         = [ 'borg', 'sshfs' ],
  String  $sshfs_mount  = '/mnt',
  String  $user         = 'root',
  String  $cron_time    = '0 1 * * *' ) {

  file {
    "/usr/local/sbin/borg-${name}":
      content          => epp('borg/backups.epp', {
        'name'         => $name,
        'backup_path'  => $backup_path,
        'archive_name' => $archive_name,
        'archive_path' => $archive_path,
        'passphrase'   => $passphrase,
        'exclude'      => $exclude,
        'prune'        => $prune,
        'daily'        => $daily,
        'weekly'       => $weekly,
        'monthly'      => $monthly,
        'yearly'       => $yearly,
        'log'          => $log,
        'type'         => $type,
        'sshfs_mount'  => $sshfs_mount,
        'user'         => $user,
        'cron_time'    => $cron_time,
      }),
      owner   => $user,
      group   => $user,
      mode    => '0700';

    "/etc/cron.d/borg-${name}":
      ensure  => file,
      content => "${cron_time} ${user} /usr/local/sbin/borg-${name}\n",
      owner   => 'root',
      group   => 'root',
      mode    => '0644';
  }
}
